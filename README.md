# Writing applications in Angular 2, [part 10] #

## Routing in Angular 2 ##

Routing in Angular 2 is really powerfull, so we will make more then one stop here. Routing in Angular 2 can be divided into these categories:

* **Basic routing** (Viewing of components in the place of router-outlet tag, based on the click at the router link)
* **Nested child routes** (Yeah [EmberJS](http://emberjs.com/) guys, our nested routes!..:)
* **Aux Routes** Ability of having more then one named router-outlet. Cool!

## Basic routing ##

In this post, let's show the basic ability of Angular 2's routing. In our example of simple products app, 
**let's create a sidebar with two links**. One leading to product-form designated for creation of new products
and the second for showing actual product list. Css **styles are going to be based on the [twitter bootstrap](http://getbootstrap.com/2.3.2/)** 
and the sidebar template is from [startbootstrap-simple-sidebar](https://github.com/BlackrockDigital/startbootstrap-simple-sidebar)

Basic routing in Angular 2 is comprised of following steps:

**Defining of RouterConfig array**. In our example, let's define the mentioned links to product-form and product list components.

```
export const routes: RouterConfig = [
  { path: 'productList', component: ProductsComponent },
  { path: 'newProducts', component: ProductFormComponent }
];
```
By this code we're saying:" Whenever we visit <baseURL>/productList then ProductsComponent is shown. And whenever <baseURL>/newProducts
is visited then ProductFormComponent is shown.

**Exporting of routes** 

Router can be used by more then one application. To actually use this feature, a programmer needs to export the RouterConfig routes outside 
of the class where the routes are defined.

```
export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
```
**Binding the router to application**.

To bind the router to application, bootstrapping of root component needs to be changed to:

```
import { bootstrap }    from '@angular/platform-browser-dynamic';
import { AppComponent } from './app.component';
import { APP_ROUTER_PROVIDERS } from './app.router';

bootstrap(AppComponent, [APP_ROUTER_PROVIDERS]);
```

And that's all. I mentioned the sidebar, so let's change the AppComponent to be just showing a sidebar **with the two routerLinks**
and** router-outlet tag**, which is the tag where component, binded to path in RouterConfig is going to be drawn.


```
import { Component } from '@angular/core';
import { ProductService } from './product.service';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    providers: [ProductService],
    directives: [ROUTER_DIRECTIVES]
})
export class AppComponent {
}
```
This is the app's root component. We defined here a provider for ProductService, to have the service instance sharing working.
And also, we're going to be drawing Angular's 2 router links, so ROUTER_DIRECTIVES definition is necessary. 

**app.component.html**:

```
<div id="wrapper">
   <!-- Sidebar -->
   <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
           <li>
               <a [routerLink]="['/productList']">Product List</a>
           </li>
           <li>
               <a [routerLink]="['/newProducts']">Product Form</a>
           </li>
        </ul>
    </div>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12"> 
                    <router-outlet></router-outlet>
                </div>
            </div>
        </div>
    </div>
</div>
```
We defined here two **routerLinks**, one for product-form and one for productList. By **router-outlet** tag, we defined place were 
ProductFormComponent(URL "newProducts", see RouterConfig) and ProductsComponent(URL "productList", see RouterConfig) components are going to be drawn.

# Testing the demo #

* git clone <this repo>
* npm install
* npm start
* visit localhost:3000

regards

Tomas