"use strict";
var router_1 = require('@angular/router');
var product_form_component_1 = require('./product-form.component');
var products_component_1 = require('./products.component');
exports.routes = [
    { path: 'productList', component: products_component_1.ProductsComponent },
    { path: 'newProducts', component: product_form_component_1.ProductFormComponent }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
//# sourceMappingURL=app.router.js.map