import { provideRouter, RouterConfig } from '@angular/router';

import { ProductFormComponent } from './product-form.component';
import { ProductsComponent } from './products.component';
import { AppComponent } from './app.component';

export const routes: RouterConfig = [
  { path: 'productList', component: ProductsComponent },
  { path: 'newProducts', component: ProductFormComponent }
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];