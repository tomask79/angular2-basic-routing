import {Injectable} from '@angular/core';
import {ProductType} from './product';


@Injectable()
export class ProductService {
    private _product_list = [
                new ProductType('ATI Radeon', 5000, 'http://pctuning.tyden.cz/ilustrace3/Sulc/radeon_hd5870/hd4870.jpg'),
                new ProductType('NVidia GTX 750', 6000, 'http://www.gigabyte.com.au/News/1272/2.jpg')
                ]; 
    public getProducts() : ProductType[] {        
        return this._product_list;
    }

    public addProduct(product: ProductType) {
        this._product_list.push(product);
    }
}