import {Component, OnInit} from '@angular/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {ProductDetailComponent} from './product.detail.component';
import {ProductFormComponent} from './product-form.component';

@Component({
    selector: 'products',
    template: `     
              <ul>
                  <li *ngFor="let product of _products">
                     <product-detail [product]="product"></product-detail>
                  </li>
              </ul>       
    `,
    directives: [ProductDetailComponent]
})
export class ProductsComponent implements OnInit {
    private _products: ProductType[];

    constructor(private productService: ProductService) {
    }

    ngOnInit() : void {
        this._products = this.productService.getProducts();
    }
}