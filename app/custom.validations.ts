import {AbstractControl} from '@angular/forms';

export class CustomValidators {
  static priceValueValidation(control: AbstractControl): { [s: string]: boolean } {
    // Validation is OK only if control is not changed or has number bigger > 0 otherwise throw error.
    return (control.pristine || parseInt(control.value)) > 0 ? null : {"lessThenZeroPriceError": true};
  }
}