import { Component } from '@angular/core';
import { ProductService } from './product.service';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    providers: [ProductService],
    directives: [ROUTER_DIRECTIVES]
})
export class AppComponent {
}